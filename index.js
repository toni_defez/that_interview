const groups = [
    {
      id: 1,
      name: 'admins',
      permissions: ['CREATE', 'DELETE', 'READ', 'WRITE'],
    },
    {
      id: 2,
      name: 'readers',
      permissions: ['READ'],
    },
    {
      id: 3,
      name: 'writers',
      permissions: ['WRITE'],
    },
    {
      id: 4,
      name: 'managers',
      permissions: ['CREATE', 'DELETE'],
    }
  ];
  
  const users = [
    {
      name: 'john',
      groups: [1],
    },
    {
      name: 'mary',
      groups: [2, 3],
    },
    {
      name: 'alice',
      groups: [2]
    },
    {
      name: 'bob',
      groups: [3],
    },
    {
      name: 'eve',
      groups: [2, 4],
    },
  ];
  
  /*
    TODO: Given the name of an user and a required permission, return true if
    the user have that permission and false otherwise.
  */
  
  // MAYBE NEXT TIME 
  const userHasPermission = (userName, requiredPermission) => {
    let currentUser = users.find(item=>item.name === userName);
    let currentPermisions = 
        groups.filter( 
          group=>currentUser.groups.includes(group.id)
        ).map(item =>{ 
          return item.permissions
        } );
    
    
    let hasPermission = false;
      currentPermisions.forEach(item=>{
        if( item.includes(requiredPermission)){
          hasPermission = true;
        }
      });
    return hasPermission;
  };
  
  console.log('Can john DELETE? ', userHasPermission('john', 'DELETE'));
  console.log('Can mary DELETE? ', userHasPermission('mary', 'DELETE'));
  console.log('Can alice WRITE? ', userHasPermission('alice', 'WRITE'));
  console.log('Can bob WRITE? ', userHasPermission('bob', 'WRITE'));
  console.log('Can eve CREATE? ', userHasPermission('eve', 'CREATE'));